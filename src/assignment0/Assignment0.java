/*
 * Assignment 0, Memory matching game
 * Vince Belanger
 * CPS 151
 */
package assignment0;

import java.util.Random;
import java.util.Scanner;

public class Assignment0 {
    
    //Generates random number
    public static int rand(){
        Random rn = new Random();
        int range = 8; //Random number of 1-8
        int randomNum =  rn.nextInt(range) + 1;
        return randomNum;
    }
    
    //Checks if the number is in the cardTable array twice or not, returns boolean
    public static boolean checkNum(int cardTable[][], int randomNum){
        int counter = 0;
        for (int x = 0; x < cardTable.length; x++) { //Loops for each row
            for (int y = 0; y < cardTable[x].length; y++) { //Loops for each entry in the row
               if (randomNum == cardTable[x][y]) //If number is in array, increase counter
                  counter++;
               if (counter == 2) //If counter reaches 2, return that the number has been used twice in the array
                  return true;
            }
        }
        return false; //If number is not in the array exactly twice, method checkNum will return false
    }
    
    //method to print table
    public static void printBoard(int table[][]){
            System.out.println("  1 2 3 4"); //Column headers
            for (int x = 0; x < table.length; x++) { //4 times, for each row
                System.out.print(x + 1); //Row header
                     for (int y = 0; y < table[x].length; y++) { //4 times, for each entry in the row
                        if (table[x][y] == 0) //If the array value is 0, print out * instead
                           System.out.print(" *");
                        else  //Else, print out the number
                          System.out.print(" " + table[x][y]);
                     }
                     System.out.println();
            }
    }
    
    // Main method
    public static void main(String[] args) throws Exception {
        int pairsFound = 0; //pairs found. When this gets to 8, player wins
        int[][] cardTable = new int[4][4]; //Initializes actual board array
        int[][] visibleBoard = new int[4][4]; //Initializes visible board array
        boolean twoExisting;
        int randomNum = 0; //Initializes random number
        
        for (int x = 0; x < cardTable.length; x++) { //4 times, for each row
         for (int y = 0; y < cardTable[x].length; y++) { //4 times, for each entry in the row
            do{ //Gets random number and from the rand() method and checks if it exists in the array
                randomNum = rand();
                twoExisting = checkNum(cardTable, randomNum); //checknum returns boolean value to store in twoExisting
            }
            while(twoExisting);
            cardTable[x][y] = randomNum; //Sets array position equal to a randomly generated number from the rand method
         }
        }  
        
        Scanner cin = new Scanner(System.in);
        while(pairsFound < 8){ //When a pair is found, this increments. Starts at 0
            String coord1; //Initializing user input #1
            String coord2; //Initializing user input #2

            printBoard(visibleBoard); //Prints the visible board
            
            System.out.print("Enter the first coordinate pair (x y); each coordinate is in 1...4: ");
            coord1 = cin.nextLine();
            System.out.print("Enter the second coordinate pair (x y); each coordinate is in 1...4: ");
            coord2 = cin.nextLine();
            
            //Converting input into chars
            char CfirstPairRow = coord1.charAt(0);
            char CfirstPairColumn = coord1.charAt(2);
            char CsecondPairRow = coord2.charAt(0);
            char CsecondPairColumn = coord2.charAt(2);
            
            //Converting those chars into ints for coordinates
            int firstPairRow = Character.getNumericValue(CfirstPairRow) - 1;
            int firstPairColumn = Character.getNumericValue(CfirstPairColumn) - 1;
            int secondPairRow = Character.getNumericValue(CsecondPairRow) - 1;
            int secondPairColumn = Character.getNumericValue(CsecondPairColumn) - 1;
            
            //Throwing exceptions. 0-3 because of array index
            if((firstPairRow < 0) || (firstPairRow > 3))
                throw new Exception("First X coordinate (" + firstPairRow + ") is out of range or is not a number.");
            if((firstPairColumn < 0) || (firstPairColumn > 3))
                throw new Exception("First Y coordinate (" + firstPairColumn + ") is out of range or is not a number.");
            if((secondPairRow < 0) || (secondPairRow > 3))
                throw new Exception("Second X coordinate (" + secondPairRow + ") is out of range or is not a number.");
            if((secondPairColumn < 0) || (secondPairColumn > 3))
                throw new Exception("Second Y coordinate (" + secondPairColumn + ") is out of range or is not a number.");
            
            //Check if two coords are the same. If they are, update visibleBoard in both locations
            try{
            if(cardTable[firstPairRow][firstPairColumn] == cardTable[secondPairRow][secondPairColumn]){
                visibleBoard[firstPairRow][firstPairColumn] = cardTable[firstPairRow][firstPairColumn];
                visibleBoard[secondPairRow][secondPairColumn] = cardTable[secondPairRow][secondPairColumn];
                pairsFound++;
                printBoard(visibleBoard);
            }
            //If the two coords are not the same, update visibleBoard to display it temporarily, then change them back to 0 (*)
            else{
                visibleBoard[firstPairRow][firstPairColumn] = cardTable[firstPairRow][firstPairColumn];
                visibleBoard[secondPairRow][secondPairColumn] = cardTable[secondPairRow][secondPairColumn];
                printBoard(visibleBoard);
                visibleBoard[firstPairRow][firstPairColumn] = 0;
                visibleBoard[secondPairRow][secondPairColumn] = 0;          
            }
            }
            //Catches the exceptions if the numbers are out of the 1-4 range
            catch(Exception e){
                System.out.println(e.getMessage());
            }
            
            //Print out tempVisibleBoard here.
            System.out.println("Press Enter to continue...");
            cin.nextLine();
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        }
        System.out.println("Congratulations, you win!");
        printBoard(visibleBoard); //Prints completed board, visibleBoard is the same as cardTable here
    }
}